#include "genabc.h"

#include <algorithm>
#include <fstream>
#include <chrono>
#include <iomanip>

#include "logger.h"
#include "platform.h"

namespace luteconv
{

// all flags are relative to this
const NoteType baseNoteType{NoteTypeWhole};

void GenAbc::Generate(const Options& options, const Piece& piece)
{
    std::fstream dst;
    dst.open(options.m_dstFilename.c_str(), std::fstream::out | std::fstream::trunc);
    if (!dst.is_open())
        throw std::runtime_error(std::string("Error: Can't open ") + options.m_dstFilename);
    
    Generate(options, piece, dst);
}

void GenAbc::Generate(const Options& options, const Piece& piece, std::ostream& dst)
{
    LOGGER << "Generate abc";
    
    // Grid iron style?
    bool gridiron{false};
    for (const auto & bar : piece.m_bars)
    {
        for (const auto & chord : bar.m_chords)
        {
            if (chord.m_grid != GridNone)
            {
                gridiron = true;
                break;
            }
        }
        if (gridiron)
            break;
    }
     
    std::time_t tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    struct std::tm * ptm = std::gmtime(&tt);
    
    dst << "X:1" << std::endl;

    // pseudo comments
    if (gridiron)
        dst << "%%tabrhstyle grid" << std::endl;
    
    dst << "%%tabaddflags 0" << std::endl;   // don't mess with the flags!

    if (!piece.m_title.empty())
        dst << "T:" << piece.m_title << std::endl;

    if (!piece.m_composer.empty())
        dst << "C:" << piece.m_composer << std::endl;

    if (!piece.m_copyright.empty())
        dst << "N:" << piece.m_copyright << std::endl;
    
    for (const auto & credit : piece.m_credits)
        dst << "N:" << credit << std::endl;
    
    const size_t slash = options.m_dstFilename.find_last_of(pathSeparator);
    if (slash == std::string::npos)
        dst << "F:" << options.m_dstFilename << std::endl;
    else
        dst << "F:" << options.m_dstFilename.substr(slash + 1) << std::endl;

    static_assert(baseNoteType == NoteTypeWhole, "");
    dst << "L:1" << std::endl;
    
    dst << "Z:Converted to abc .abc by luteconv " << options.m_version << " encoding-date " << std::put_time(ptm,"%F") << std::endl;

    switch (options.m_dstTabType)
    {
    case TabNeopolitan:
    case TabUnknown:
    case TabFrench:
        dst << "K:frenchtab" << std::endl;
        break;
    case TabGerman:
        dst << "K:germantab" << std::endl;
        break;
    case TabItalian:
        if (options.m_tuning.size() <= 6)
            dst << "K:italiantab" << std::endl;
        else if (options.m_tuning.size() == 6)
            dst << "K:italian7tab" << std::endl;
        else
            dst << "K:italian8tab" << std::endl;
        break;
    case TabSpanish:
        dst << "K:spanishtab" << std::endl;
        break;
    }
    
    // body
    int staveNum{1};
    int barNum{1};
    int chordCount{0};
    std::string repForward;
    
    dst << "% Stave " << staveNum <<  std::endl;
    
    for (const auto & bar : piece.m_bars)
    {
        if (chordCount == 0)
        {
            // first bar on line
            dst <<  "% Bar " << barNum << std::endl;
            if (!repForward.empty())
            {
                dst << repForward << "\\" << std::endl;
                repForward.clear();
            }
            else
            {
                dst << "|" << "\\" << std::endl;
            }
        }
        
        // time signature
        const std::string timeSignature = GetTimeSignature(bar);
        if (!timeSignature.empty())
            dst << timeSignature << "\\" << std::endl;
        
        // chords
        for (const auto & chord : bar.m_chords)
        {
            ++chordCount;
            
            std::vector<std::string> vert;
            vert.reserve(6);
            std::string diapason;
            
            vert.emplace_back(","); // in case no notes 
            
            for (const auto & note : chord.m_notes)
            {
                if (note.m_string <= 6)
                {
                    while (static_cast<int>(vert.size()) < note.m_string)
                        vert.emplace_back(",");
                    
                    // abc only supports right ornaments, if none use left ornament instead
                    const std::string ornament = GetOrnament((note.m_rightOrnament != OrnNone)
                                                    ? note.m_rightOrnament
                                                    : note.m_leftOrnament);

                    vert[note.m_string - 1] = ornament + GetFret(note);
                }
                else
                {
                    diapason = GetFret(note);
                }
            }
            
            std::string tabWord;
            
            if (chord.m_fermata)
                tabWord += "H";
            
            // abc only supports right hand fingering on lowest plucked course
            if (!chord.m_notes.empty())
                tabWord += GetRightFingering(chord.m_notes.back());
            
            tabWord += "[";  // always use chord notation

            for (const auto & s : vert)
            {
                tabWord += s;
            }
            
            tabWord += diapason
                    + GetFlagInfo(options, bar.m_chords, chord) 
                    + "]";
            
            // end of grid marked by a space
            if (gridiron && chord.m_grid != GridStart && chord.m_grid != GridMid)
                tabWord += " ";
            
            dst << tabWord << "\\" << std::endl;
        }
        
        // end of current bar
        ++barNum;
        dst <<  "% Bar " << barNum << std::endl;
        
        // Stave ending Use herustic:
        // count chords, when the threshold is reached end the stave at the end of
        // the current bar.  Except for last bar.
        const bool lineBreak = barNum < static_cast<int>(piece.m_bars.size()) && chordCount > options.m_wrapThreshold;
        
        std::string barStyle;
        switch (bar.m_barStyle)
        {
        case BarStyleLightLight:
            barStyle = "||";
            break;
        case BarStyleLightHeavy:
            barStyle = "|]";
            break;
        case BarStyleHeavyLight:
            barStyle = "[|";
            break;
        default:
            barStyle = "|";
        }
        
        switch (bar.m_repeat)
        {
        case RepNone:
            break;
        case RepForward:
            barStyle = "|:";
            break;
        case RepBackward:
            barStyle = ":|";
            break;
        case RepJanus:
            if (lineBreak)
            {
                // backward repeat here, forward repeat in next bar, next line
                repForward = "|:";
                barStyle = ":|";
            }
            else
            {
                barStyle = "::";
            }
        }
        
        if (lineBreak)
        {
            dst << barStyle << std::endl;
            ++staveNum;
            chordCount = 0;
            dst << "% Stave " << staveNum << std::endl;
        }
        else
        {
            dst << barStyle << "\\" << std::endl;
        }
    }
}
    
std::string GenAbc::GetTimeSignature(const Bar & bar)
{
    std::string timeSig;
    
    switch (bar.m_timeSig.m_timeSymbol)
    {
    case TimeSyNone:
        return "";
    case TimeSyCommon:
        timeSig = "M:C";
        break;
    case TimeSyCut:
        timeSig = "M:C|";
        break;
    case TimeSySingleNumber:
        timeSig = "M:" + std::to_string(bar.m_timeSig.m_beats);
        break;
    case TimeSyNote:
        return "";
    case TimeSyDottedNote:
        return "";
    case TimeSyNormal:
        timeSig = "M:" + std::to_string(bar.m_timeSig.m_beats) + "/" + std::to_string(bar.m_timeSig.m_beatType);
        break;
    }
    return "[" + timeSig + "]";
}

std::string GenAbc::GetFlagInfo(const Options& options, const std::vector<Chord> & chords, const Chord & our)
{
    if (our.m_noFlag)
        return "";

    int num{1};
    int den{1};
    if (our.m_noteType > baseNoteType)
        den <<= our.m_noteType - baseNoteType;
    else if (our.m_noteType < baseNoteType)
        num <<= baseNoteType - our.m_noteType;

    if (our.m_dotted)
    {
        num *= 3;
        den *= 2;
        const int gcd = Gcd(num, den);
        num /= gcd;
        den /= gcd;
    }

    std::string ourFlags;
    if (den == 1)
    {
        // multiplier, no divisor
        ourFlags += std::to_string(num);
    }
    else if (num == 1 && den == 2)
    {
        // 1/2 => /
        ourFlags += "/";
    }
    else if (num == 1)
    {
        // 1/den => /den
        ourFlags += "/" + std::to_string(den);
    }
    else
    {
        // num/den
        ourFlags += std::to_string(num) + "/" + std::to_string(den);
    }
    
    return ourFlags;
}

int GenAbc::Gcd(int a, int b)
{
    return (b == 0) ? a : Gcd(b, a % b);
}

std::string GenAbc::GetRightFingering(const Note & note)
{
    switch (note.m_rightFingering)
    {
    case FingerNone:
        return "";
    case FingerFirst:
        return ".";
    case FingerSecond:
        return ":";
    case FingerThird:
        return ";";
    case FingerForth:
        return "";
    case FingerThumb:
        return "+";
    }
    return "";
}
    
std::string GenAbc::GetOrnament(Ornament ornament)
{
    switch (ornament)
    {
    case OrnNone:
        return "";
    case OrnHash:
        return "#";
    case OrnPlus:
        return "";
    case OrnCross:
        return "X";
    case OrnLeftDot:
        return "";
    case OrnBrackets:
        return "";
    case OrnComma:
        return "'";
    }
    return "";
}

std::string GenAbc::GetFret(const Note & note)
{
    // a..p, excluding j
    const std::string letter{static_cast<char>('a' + note.m_fret + (note.m_fret > 8 ? 1 : 0))};
    
    if (note.m_string >= 11)
    {
        return "{" + std::to_string(note.m_string - 7) + "}";
    }
    else if (note.m_string >= 7)
    {
        return "{" + std::string(note.m_string - 7, ',') + letter + "}";
    }
    else
    {
        return letter;
    }
}

} // namespace luteconv
