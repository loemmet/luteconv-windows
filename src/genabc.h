#ifndef _GENABC_H_
#define _GENABC_H_

#include <iostream>
#include <string>

#include "piece.h"
#include "options.h"

namespace luteconv
{

/**
 * Generate .tab
 */
class GenAbc
{
public:
    /**
     * Constructor
     */
    GenAbc() = default;

    /**
     * Destructor
     */
    ~GenAbc() = default;
    
    /**
     * Generate abc .abc from destination file in options
     * 
     * @param[in] options
     * @param[in] piece
     */
    void Generate(const Options& options, const Piece& piece);

    /**
     * Generate abc .abc
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    void Generate(const Options& options, const Piece& piece, std::ostream& dst);
    
private:
    static std::string GetTimeSignature(const Bar & bar);
    static std::string GetFlagInfo(const Options& options, const std::vector<Chord> & chords, const Chord & our);
    static std::string GetRightFingering(const Note & note);
    static std::string GetOrnament(Ornament ornament);
    static std::string GetFret(const Note & note);
    static int Gcd(int a, int b);
};

} // namespace luteconv

#endif // _GENABC_H_

