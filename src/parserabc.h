#ifndef _PARSERABC_H_
#define _PARSERABC_H_

#include <iostream>

#include "piece.h"
#include "options.h"

namespace luteconv
{

/**
 * Parse AbcTab .abc file
 * 
 * http://www.lautengesellschaft.de/cdmm/
 */
class ParserAbc
{
public:

    /**
     * Constructor
    */
    ParserAbc() = default;

    /**
     * Destructor
     */
    ~ParserAbc() = default;
    
    /**
     * Parse .abc file
     *
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(const Options& options, Piece& piece);
    
    /**
     * Parse .abc file
     *
     * @param[in] src .abc stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece);
    
private:
    void ParseHeader(const std::string& line, int lineNo, Piece& piece);
    void ParseMusic(const std::string& line, int lineNo, Bar& bar, bool& barIsClear, Piece& piece);
    void ParseBarLine(const std::string& line, int lineNo, size_t& idx, Bar& bar, bool& barIsClear, Piece& piece);
    void ParseChord(const std::string& line, int lineNo, bool& fermata, Fingering& fingering, bool& nextHalved,
            bool& nextDotted, size_t& idx, Bar& bar, bool& barIsClear);
    void ParseRhythm(const std::string &line, int lineNo, size_t &idx, NoteType baseNoteType, NoteType& noteType, bool& dotted);
    void ParseInformation(const std::string& line, int lineNo, size_t& idx, Bar& bar);
    void ParseTimeSignature(const std::string& line, int lineNo, size_t idx, TimeSig& timeSig);
    
    NoteType m_defaultNoteType{NoteTypeWhole};
    TimeSig m_timeSig;
};

} // namespace luteconv

#endif // _PARSERABC_H_
