#include "parserabc.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <cctype>
#include <chrono>
#include <iomanip>
#include <regex>

#include "logger.h"
#include "platform.h"

namespace luteconv
{

void ParserAbc::Parse(const Options &options, Piece &piece)
{
    std::fstream src;
    src.open(options.m_srcFilename.c_str(), std::fstream::in);
    if (!src.is_open())
        throw std::runtime_error("Error: Can't open " + options.m_srcFilename);
    Parse(src, options, piece);
}

void ParserAbc::Parse(std::istream &src, const Options &options, Piece &piece)
{
    LOGGER << "Parse abc";

    int lineNo{0};
    Bar bar;
    bool barIsClear{true};
    bool inHeader{true};

    // find our tune
    try
    {
        bool found{false};
        const std::string tuneRef{"X:" + std::to_string(std::stoi(options.m_index) + 1)};
        while (!src.eof() && !found)
        {
            std::string line;
            getline(src, line);
            ++lineNo;
            found = (line.substr(0, tuneRef.size()) == tuneRef);
        }
        
        if (found)
            LOGGER << lineNo << ": found tune " << tuneRef;
        else
            throw std::runtime_error(("Error: can't find tune " + tuneRef).c_str());
    }
    catch (...)
    {
        throw std::runtime_error(("Error: option --index=" + options.m_index + " is not a number").c_str());
    }

    while (!src.eof())
    {
        std::string line;
        getline(src, line);
        ++lineNo;

        // Each tune starts with an ”X:” (reference number) info field and is terminated by a blank line or the end of the file.
        if (line.empty())
            break;

        // pseudo comments
        if (line.size() >= 2 && line[0] == '%' && line[1] == '%')
        {
            LOGGER << lineNo << ": ignore pseudo comment: " << line;
            continue;
        }

        // strip comments, both start of line and end of line comments
        const auto percent = line.find_first_of('%');
        if (percent != std::string::npos)
        {
            line = line.substr(0, percent);
            if (line.empty())
                continue;
        }

        if (inHeader)
        {
            if (line.size() >= 2 && isalpha(line[0]) && line[1] == ':')
            {
                ParseHeader(line, lineNo, piece);
                continue;
            }
            else
            {
                inHeader = false;
            }
        }

        ParseMusic(line, lineNo, bar, barIsClear, piece);

        // end of system if line ends without a backslash
        if (line[line.size() - 1] != '\\' && barIsClear && !piece.m_bars.empty())
        {
            Bar &prev = piece.m_bars.back();
            prev.m_eol = true;
        }
    }

    // deal with missing final bar line
    const std::string line{"|]"};
    size_t idx{0};
    ParseBarLine(line, lineNo, idx, bar, barIsClear, piece);

    // if there is a time signature in the header, but not in the first bar
    // then put it in the first bar
    if (m_timeSig.m_timeSymbol != TimeSyNone && !piece.m_bars.empty() && piece.m_bars.front().m_timeSig.m_timeSymbol == TimeSyNone)
        piece.m_bars.front().m_timeSig = m_timeSig;
    
    piece.SetTuning(options);
}

void ParserAbc::ParseHeader(const std::string &line, int lineNo, Piece &piece)
{
    switch (line[0])
    {
    case 'T':   // title
        if (piece.m_title.empty())
        {
            piece.m_title = line.substr(2);
            LOGGER << lineNo << ": title=" << piece.m_title;
        }
        else
        {
            piece.m_credits.emplace_back(line.substr(2));
        }
        break;
    case 'C':   // composer
        if (piece.m_composer.empty())
        {
            piece.m_composer = line.substr(2);
            LOGGER << lineNo << ": composer=" << piece.m_title;
        }
        else
        {
            piece.m_credits.emplace_back(line.substr(2));
        }
        break;
    case 'L':   // default note length
    {
        size_t idx{2};
        bool dotted{false};
        ParseRhythm(line, lineNo, idx, NoteTypeWhole, m_defaultNoteType, dotted);
        LOGGER << lineNo << ": default note type=" << m_defaultNoteType << " " << line;
        break;
    }
    case 'K':   // key
    {
        // "frenchtab" "french4tab" "french5tab" "spanishtab" "guitartab" "spanish5tab"
        // "banjo5tab" "spanish4tab", "banjo4tab" "ukuleletab" "italiantab" "italian7tab"
        // "italian8tab" "italian4tab" "italian5tab" "germantab"
        const std::regex re{R"(french[45]?tab|germantab|italian[4578]?tab|spanish[45]?tab)"};
        std::cmatch results;
        if (!std::regex_search(line.c_str(), results, re))
        {
            throw std::runtime_error(("Error: no tablature type specified " + line).c_str());
        }
        break;
    }
    case 'A':
    case 'B':
    case 'D':
    case 'F':
    case 'G':
    case 'H':
    case 'N':
    case 'O':
    case 'S':
        // dump these into credits
        piece.m_credits.emplace_back(line.substr(2));
        break;
    case 'M':
        ParseTimeSignature(line, lineNo, 0, m_timeSig);
        break;
    default:
        LOGGER << lineNo << ": ignore header " << line;
        break;
    }
}

void ParserAbc::ParseMusic(const std::string &line, int lineNo, Bar &bar, bool &barIsClear, Piece &piece)
{
    size_t idx{0}; // character pointer
    Fingering fingering{FingerNone}; // fingering for last note in chord
    bool fermata{false};
    bool nextHalved{false};
    bool nextDotted{false};

    while (idx < line.size())
    {
        // skip whitespace
        if (line[idx] == ' ' || line[idx] == '\t')
        {
            ++idx;
            continue;
        }

        switch (line[idx])
        {
        case '|':
            ParseBarLine(line, lineNo, idx, bar, barIsClear, piece);
            break;
        case ':':
            // either a bar line or righthand fingering
            if (idx + 1 < line.size() && (line[idx + 1] == '|' || line[idx + 1] == ':'))
            {
                ParseBarLine(line, lineNo, idx, bar, barIsClear, piece);
            }
            else
            {
                fingering = FingerSecond;
                ++idx;
            }
            break;
        case '[':
            // either chord, or barline, or information field
            if (idx + 1 < line.size() && line[idx + 1] == '|')
            {
                ParseBarLine(line, lineNo, idx, bar, barIsClear, piece);
            }
            else if (idx + 2 < line.size() && line[idx + 2] == ':')
            {
                ParseInformation(line, lineNo, idx, bar);
            }
            else
            {
                ParseChord(line, lineNo, fermata, fingering, nextHalved, nextDotted, idx, bar, barIsClear);
            }
            break;
        case 'H':
            fermata = true;
            ++idx;
            break;
        case '.':
            fingering = FingerFirst;
            ++idx;
            break;
        case ';':
            fingering = FingerThird;
            ++idx;
            break;
        case '+':
            fingering = FingerThumb;
            ++idx;
            break;
        case 'T': // trill
        case 'S': // segno sign
        case 'O': // coda sign
            ++idx;
            break;
        case '!':
            ++idx;
            while (idx < line.size() && line[idx] != '!')
                ++idx;
            ++idx;
            break;
        case '\"':
            ++idx;
            while (idx < line.size() && line[idx] != '\"')
                ++idx;
            ++idx;
            break;
        case '\\': // stave continuation
            ++idx;
            break;
        case '>': // ”>” means the previous note is dotted, the next note halved
            if (!bar.m_chords.empty())
            {
                bar.m_chords.back().m_dotted = true;
                nextHalved = true;
            }
            ++idx;
            break;
        case '<': // "<" means ‘the previous note is halved, the next dotted’
            if (!bar.m_chords.empty())
            {
                bar.m_chords.back().m_noteType = static_cast<NoteType>(bar.m_chords.back().m_noteType + 1);
                nextDotted = true;
            }
            ++idx;
            break;
        case '(': // ties and slurs
        case ')':
        case 'y':
            ++idx;
            break;
        default:
        {
            // Single note chord, no [ ... ]
            ParseChord(line, lineNo, fermata, fingering, nextHalved, nextDotted, idx, bar, barIsClear);
            break;
        }
        }
    }
}

void ParserAbc::ParseBarLine(const std::string &line, int lineNo, size_t &idx, Bar &bar, bool &barIsClear, Piece &piece)
{
    if (line.substr(idx, 3) == "[|]")
    {
        bar.m_barStyle = BarStyleRegular;
        idx += 3;
    }
    else if (line.substr(idx, 2) == "|]")
    {
        bar.m_barStyle = BarStyleLightHeavy;
        idx += 2;
    }
    else if (line.substr(idx, 2) == "||")
    {
        bar.m_barStyle = BarStyleLightLight;
        idx += 2;
    }
    else if (line.substr(idx, 2) == "[|")
    {
        bar.m_barStyle = BarStyleHeavyLight;
        idx += 2;
    }
    else if (line.substr(idx, 2) == ":|")
    {
        bar.m_barStyle = BarStyleRegular;
        bar.m_repeat = RepBackward;
        idx += 2;
    }
    else if (line.substr(idx, 2) == "|:")
    {
        bar.m_barStyle = BarStyleRegular;
        bar.m_repeat = RepForward;
        idx += 2;
    }
    else if (line.substr(idx, 2) == "::")
    {
        bar.m_barStyle = BarStyleRegular;
        bar.m_repeat = RepJanus;
        idx += 2;
    }
    else if (line[idx] == '|')
    {
        bar.m_barStyle = BarStyleRegular;
        ++idx;
    }
    else
    {
        bar.m_barStyle = BarStyleRegular;
        LOGGER << lineNo << ": \"" << line << "\" unknown barline";
        ++idx;
    }

    if (barIsClear && !piece.m_bars.empty())
    {
        // two adjacent bar lines, combine
        Bar &prev = piece.m_bars.back();
        if (prev.m_repeat == RepBackward && bar.m_repeat == RepForward)
        {
            prev.m_repeat = RepJanus;
        }

        if (!prev.m_eol)
        {
            if (prev.m_barStyle == BarStyleRegular && bar.m_barStyle == BarStyleRegular)
            {
                prev.m_barStyle = BarStyleLightLight;
            }
            else if (prev.m_barStyle == BarStyleRegular && bar.m_barStyle == BarStyleHeavy)
            {
                prev.m_barStyle = BarStyleLightHeavy;
            }
            else if (prev.m_barStyle == BarStyleHeavy && bar.m_barStyle == BarStyleRegular)
            {
                prev.m_barStyle = BarStyleHeavyLight;
            }
            else if (prev.m_barStyle == BarStyleHeavy && bar.m_barStyle == BarStyleHeavy)
            {
                prev.m_barStyle = BarStyleHeavyHeavy;
            }
        }
    }
    else if (!barIsClear)
    {
        piece.m_bars.push_back(bar);
    }

    bar.Clear();
    barIsClear = true;
}

void ParserAbc::ParseChord(const std::string &line, int lineNo, bool& fermata,
        Fingering& fingering, bool& nextHalved, bool& nextDotted, size_t &idx,
        Bar &bar, bool& barIsClear)
{
    const size_t startIdx{idx};
    
    bar.m_chords.emplace_back(); // new chord
    Chord &chord = bar.m_chords.back();
    
    bool gotRhythm{false};
    int string{1};
    static Chord previousChord;
    bool inChord{false};
    if (line[idx] == '[')
    {
        inChord = true;
        ++idx;
    }
    
    while (idx < line.size() && line[idx] != ']')
    {
        Ornament ornament = OrnNone;
        
        for (;;)
        {
            // unplayed strings
            while (idx < line.size() && line[idx] == ',')
            {
                ++idx;
                ++string;
            }
    
            if (idx < line.size() && (line[idx] == '(' || line[idx] == ')'))
                ++idx;
            
            // ornament
            switch (line[idx])
            {
            case '\'':
                ornament = OrnComma;
                ++idx;
                break;
            case 'X':
                ornament = OrnCross;
                ++idx;
                break;
            case '#':
                ornament = OrnHash;
                ++idx;
                break;
            case 'U':   // an U-shaped arc after the note
            case 'V':   // an U-shaped arc below the note
            case '*':   // an ’*’ after the note.
            case 'L':   // An oblique line under the note
                ++idx;
                break;
            default:
                break;
            }
            
            // ornaments should go against their note, but sometimes they go before the [
            if (!inChord && line[idx] == '[')
            {
                inChord = true;
                ++idx;
            }
            else
            {
                break;
            }
        }
        
        // slurs and ties
        if (idx < line.size() && (line[idx] == '(' || line[idx] == ')'))
            ++idx;

        // fret
        if (idx < line.size() && line[idx] >= 'a' && line[idx] <= 'p')
        {
            chord.m_notes.emplace_back(); // allocate new note
            Note& note = chord.m_notes.back();
            note.m_rightOrnament = ornament;

            note.m_string = string;
            note.m_fret = line[idx] - 'a' - (line[idx] <= 'i' ? 0 : 1); // fret i = j
            ++idx;
            ++string;
        }
        else if (idx < line.size() && line[idx] == '{')
        {
            // diapason
            chord.m_notes.emplace_back(); // allocate new note
            Note& note = chord.m_notes.back();
            note.m_rightOrnament = ornament;

            ++idx; // {
            if (isdigit(line[idx]))
            {
                note.m_fret = 0;
                note.m_string = 11 + line[idx] - '4';
                ++idx;
            }
            else
            {
                int string{7};
                while (idx < line.size() && line[idx] == ',')
                {
                    ++string;
                    ++idx;
                }
                note.m_string = string;
                note.m_fret = line[idx] - 'a' - (line[idx] <= 'i' ? 0 : 1); // fret i = j
                ++idx;
            }
            
            if (idx < line.size() && line[idx] == '}')
                ++idx; // }
            else
                LOGGER << lineNo << ": missing }";
        }
        
        // slurs and ties
        if (idx < line.size() && (line[idx] == '(' || line[idx] == ')'))
            ++idx;

        // rhythm?
        if (idx < line.size() && (isdigit(line[idx]) || line[idx] == '/'))
        {
            ParseRhythm(line, lineNo, idx, m_defaultNoteType, chord.m_noteType, chord.m_dotted);
            gotRhythm = true;
            break;
        }
        
        if (!inChord)
            break;
    }
    
    if (inChord)
    {
        if (line[idx] == ']')
            ++idx;
        else
            LOGGER << lineNo << ": ] missing";
    }
    
    if (idx > startIdx)
    {
        // fermata applies to chord
        chord.m_fermata = fermata;
        fermata = false;
    
        // fingering applies to last note in the chord
        if (!chord.m_notes.empty())
            chord.m_notes.back().m_rightFingering = fingering;
        fingering = FingerNone;
        
        if (!gotRhythm)
        {
            chord.m_noteType = previousChord.m_noteType;
            chord.m_dotted = previousChord.m_dotted;
            chord.m_noFlag = true;
        }
        
        // >
        if (nextHalved)
        {
            chord.m_noteType = static_cast<NoteType>(chord.m_noteType + 1);
            nextHalved = false;
        }
        
        // <
        if (nextDotted)
        {
            chord.m_dotted = true;
            nextDotted = false;
        }
        previousChord = chord;
        barIsClear = false;
    }
    else
    {
        // nothing here
        bar.m_chords.back();
        
        LOGGER << lineNo << ": unknown character \"" << line[idx] << " ignored. " << line;
        ++idx;
    }
}

void ParserAbc::ParseRhythm(const std::string &line, int lineNo, size_t &idx, NoteType baseNoteType, NoteType& noteType, bool& dotted)
{
    int num{1};
    int den{1};

    if (isdigit(line[idx]))
    {
        num = line[idx] - '0';
        ++idx;
        if (idx < line.size() && isdigit(line[idx]))
        {
            num = num * 10 + line[idx] - '0';
            ++idx;
        }
    }
    
    if (idx < line.size() && line[idx] == '/')
    {
        ++idx;
        if (idx < line.size() && isdigit(line[idx]))
        {
            den = line[idx] - '0';
            ++idx;
            if (idx < line.size() && isdigit(line[idx]))
            {
                den = den * 10 + line[idx] - '0';
                ++idx;
            }
        }
        else
        {
            den = 2;
            while (idx < line.size() && line[idx] == '/')
            {
                den <<= 1;
                ++idx;
            }
        }
    }

    dotted = (num % 3) == 0;
    if (dotted)
    {
        num *= 2;
        den *= 3;
    }

    int nt{baseNoteType};
    while (num > 1)
    {
        --nt;
        num >>= 1;
    }
    while (den > 1)
    {
        ++nt;
        den >>= 1;
    }

    noteType = static_cast<NoteType>(nt);
}


void ParserAbc::ParseInformation(const std::string &line, int lineNo, size_t &idx, Bar &bar)
{
    // [M:
    ++idx;
    if (line[idx] == 'M')
        ParseTimeSignature(line, lineNo, idx, bar.m_timeSig);
    else
        LOGGER << lineNo << ": ignore body information " << line[1];

    while (idx < line.size() && line[idx] != ']')
        ++idx;
    
    if (idx < line.size() && line[idx] == ']')
        ++idx;
    else
        LOGGER << lineNo << ": missing ]";
}

void ParserAbc::ParseTimeSignature(const std::string& line, int lineNo, size_t idx, TimeSig& timeSig)
{
    if (line.substr(idx, 6) == "M:none")
    {
        // M:none
        timeSig.m_timeSymbol = TimeSyNone;
        timeSig.m_beats = 0;
        timeSig.m_beatType = 0;
    }
    else if (line.substr(idx, 4) == "M:C|")
    {
        // M:C|
        timeSig.m_timeSymbol = TimeSyCut;
        timeSig.m_beats = 2;
        timeSig.m_beatType = 2;
    }
    else if (line.substr(idx, 3) == "M:C")
    {
        // M:C
        timeSig.m_timeSymbol = TimeSyCommon;
        timeSig.m_beats = 4;
        timeSig.m_beatType = 4;
    }
    else if (idx + 4 < line.size() && line[idx + 2] >= '1' && line[idx + 2] <= '9' && line[idx + 3] == '/' && line[idx + 4] >= '1' && line[idx + 4] <= '9')
    {
        // M:3/4
        timeSig.m_timeSymbol = TimeSyNormal;
        timeSig.m_beats = line[idx + 2] - '0';
        timeSig.m_beatType = line[idx + 4] - '0';
    }
    else if (idx + 2 < line.size() && line[idx + 2] >= '1' && line[idx + 2] <= '9')
    {
        // M:3
        timeSig.m_timeSymbol = TimeSySingleNumber;
        timeSig.m_beats = line[idx + 2] - '0';
        timeSig.m_beatType = 4;
    }
    else
    {
        LOGGER << lineNo << ":  unknown time signature. " << line;
    }
}

} // namespace luteconv
